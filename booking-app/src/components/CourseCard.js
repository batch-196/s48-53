//import { useState } from 'react';
//import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({ courseProp }) {
    // console.log(props);
    // console.log(typeof props);

    const { name, description, price, _id } = courseProp;

    // function enroll(){
    // 	if(seats > 0){
    // 		setCount(count + 1);
    // 		console.log(`Enrollees: ${count}`);
    // 		setSeats(seats - 1);
    // 		console.log(`Seats: ${seats}`);
    // 	} else {
    // 		alert('No more seats available, check back later.')
    // 	}
    // };

    return (
        <Card className="p-3 mb-3">
            <Card.Body>
                <Card.Title className="fw-bold">{name}</Card.Title>
                <Card.Subtitle> Course Description: </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle> Course Price: </Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>

    )
}

